#!/bin/sh

set -e

# Replace default END_POINT and UI_END_POINT with corresponding env variables, if provided

find /usr/share/nginx/html/assets -type f -name *.js | while read FILE; do
	if [ -n "$END_POINT" ]; then
		(set -x; sed -i "s#https://pollen-latest.demo.codelutin.com/pollen-rest-api#$END_POINT#g" "$FILE")
	fi
	if [ -n "$UI_END_POINT" ]; then
		(set -x; sed -i "s#https://pollen-ui-latest.demo.codelutin.com#$UI_END_POINT#g" "$FILE")
	fi
done
