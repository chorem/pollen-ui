FROM nginx:stable-alpine
COPY 40-replace-default-endpoints.sh /docker-entrypoint.d/
COPY ./dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
