#!/bin/bash

sudo rm -rf output

docker run --rm --name fc -v $(pwd):/work -ti -d thomaswelton/fontcustom
docker exec -it fc fontcustom compile /work/svg -c /work/fontcustom.yml -o /work/output
docker stop fc

# Installe la font générée
cp output/Pollen-Icons.ttf ../../src/assets/fonts/
cp output/Pollen-Icons.svg ../../src/assets/fonts/
cp output/Pollen-Icons.woff ../../src/assets/fonts/
cp output/Pollen-Icons.eot ../../src/assets/fonts/

# Installe le CSS généré après l'avoir rectifié
cat output/Pollen-Icons.css \
  | sed 's|url("./Pollen-Icons|url("../fonts/Pollen-Icons|g' \
  | sed 's|:before|::before|g' \
  > ../../src/assets/less/_icons.less
cat default-rules.css >> ../../src/assets/less/_icons.less

sudo rm -rf output