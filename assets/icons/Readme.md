# Mise à jour de la police d'icônes

* Ajout d'icônes au format svg dans le dossier ```assets/icons/svg```
* Exécuter le script ```fontcustom.sh```
