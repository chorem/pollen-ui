/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-typescript',
    '@vue/eslint-config-prettier/skip-formatting'
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  rules: {
    // next line deactivate 'vue/multi-word-component-names' rule to avoid linter error on single word component name
    'vue/multi-word-component-names': 'off',
    // next lines configure 'vue/no-mutating-props' rule with shallowOnly at true (default false) to avoid error on deep props mutation which is an anti pattern
    // https://vuejs.org/style-guide/rules-use-with-caution.html#implicit-parent-child-communication
    // this should be fixed
    'vue/no-mutating-props': [
      'error',
      {
        shallowOnly: true
      }
    ]
  }
}
