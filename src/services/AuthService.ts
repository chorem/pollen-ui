import { type PollenEntityRef, type PollenUser, type PollenUserBean } from '@/entities/backend'
import FetchService from '@/services/FetchService'

export function signin(login: string, password: string): Promise<PollenEntityRef<PollenUser>> {
  const url = 'login'
  const headers = {
    Authorization: 'Basic ' + btoa(unescape(encodeURIComponent(login + ':' + password)))
  }
  return FetchService.postWithHeader(url, headers, 'post')
}

export function validateEmail(userId: string, token: string) {
  const url = 'users/' + userId + '?token=' + token
  return FetchService.put(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function getAuthenticatedUser(): Promise<PollenUserBean> {
  const url = 'user'
  return FetchService.get(url).catch((e) => {
    return Promise.reject(e)
  })
}

export function isAuthenticated(): Promise<boolean> {
  return getAuthenticatedUser()
    .then((user: PollenUserBean) => {
      if (user && user.name) {
        return true
      } else {
        return false
      }
    })
    .catch(() => {
      return false
    })
}

export function logout() {
  const url = 'logout'
  return FetchService.delete(url)
    .then(() => {
      // delete local cookie
      document.cookie = 'pollen-auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;'
    })
    .catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
}
