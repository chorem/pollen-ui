import { type FeedbackBean } from '@/entities/backend'
import FetchService from '@/services/FetchService'

export function createFeedback(form: FeedbackBean): Promise<void> {
  const url = 'feedback'
  return FetchService.post(url, form).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}
