import FetchService from '@/services/FetchService'

export function isDefined(): Promise<boolean> {
  const url = 'gtu/define'
  return FetchService.get(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}
