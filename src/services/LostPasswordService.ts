import FetchService from '@/services/FetchService'

export function postLostPassword(email: string): Promise<void> {
  const url = 'lostpassword'
  return FetchService.post(url, email)
}
