import { type PollenUserBean, type PollenUserEmailAddressBean } from '@/entities/backend'
import FetchService from '@/services/FetchService'

export function getUser(userId: string): Promise<PollenUserBean> {
  const url = 'users/' + userId
  return FetchService.get(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function createAccount(user: PollenUserBean) {
  const url = 'users'
  return FetchService.post(url, user).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function saveUser(user: PollenUserBean) {
  const url = 'users/' + user.id
  return FetchService.post(url, user).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function uploadAvatar(file: File) {
  const formData = new FormData()
  formData.append('avatar', file)

  const url = 'user/avatar'

  return FetchService.post(url, formData)
    .then((res) => {
      return res
    })
    .catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
}

export function deleteAvatar() {
  const url = 'user/avatar'
  return FetchService.delete(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function addEmail(newMail: string) {
  const url = 'user/email'
  return FetchService.post(url, newMail).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function resendValidationEmail(mail: PollenUserEmailAddressBean) {
  const url = 'resendValidation'
  return FetchService.post(url, mail.emailAddress).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function deleteEmail(mail: PollenUserEmailAddressBean) {
  const url = 'user/email/' + mail.id
  return FetchService.delete(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function defaultEmail(mail: PollenUserEmailAddressBean) {
  let url = 'user/email/default'

  if (mail && mail.id) {
    url += '?emailAddressId=' + mail.id
  }

  return FetchService.put(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}

export function changePassword(changePasswordBean: any) {
  const url = 'user/password'
  return FetchService.post(url, changePasswordBean).catch((e) => {
    return Promise.reject(FetchService.parseError(e, 'user.error'))
  })
}
