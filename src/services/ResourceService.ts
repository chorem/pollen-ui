import { config } from '@/config'
import { type PollenEntityRef, type PollenResource, type ResourceType } from '@/entities/backend'
import FetchService from '@/services/FetchService'

export function getPreviewUrl(resourceId: string, maxDimension: boolean): string {
  let url = config.END_POINT + '/v1/resources/' + resourceId + '/preview'
  if (maxDimension) {
    url += '?maxDimension=true'
  }
  return url
}

export function getRessourceUrl(resourceId: string): string {
  return config.END_POINT + '/v1/resources/' + resourceId
}

export function getBuiltinRessourceUrl(builtinPicture: string): string {
  return config.UI_END_POINT + '/images/gallery/' + builtinPicture
}

export function create64(resource64: {
  data: string
  name: string
  contentType: string
  resourceType: string
}): Promise<{ permission: string; id: PollenEntityRef<PollenResource> }> {
  const url = 'resources64'
  return FetchService.post(url, resource64)
}

export function uploadFile(file: File, resourceType: ResourceType) {
  const formData = new FormData()
  formData.append('resource', file)
  formData.append('resourceType', resourceType)

  const url = config.END_POINT + '/v1/resources'

  return fetch(url, {
    method: 'POST',
    body: formData
  })
    .then((res) => {
      return res.json()
    })
    .then((res) => {
      return res
    })
    .catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
}

export function uploadCroppedFile(
  file: File,
  resourceType: ResourceType,
  width: number,
  height: number
) {
  const formData = new FormData()
  formData.append('resource', file)
  formData.append('resourceType', resourceType)

  const url = config.END_POINT + '/v1/resources?width=' + width + '&height=' + height

  return fetch(url, {
    method: 'POST',
    body: formData
  })
    .then((res) => {
      return res.json()
    })
    .then((res) => {
      return res
    })
    .catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
}

export function deleteResource(resourceId: string) {
  const url = 'resources/' + resourceId
  return FetchService.delete(url).catch((e) => {
    return Promise.reject(FetchService.parseError(e))
  })
}
