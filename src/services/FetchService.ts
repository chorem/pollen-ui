import { config } from "@/config"

export default class FetchService {
  static baseUrl = config.END_POINT + '/v1/'

  static fetch(url: string, method: string, headers: any, body?: any) {
    headers = headers || {}
    if (!headers['Content-Type'] && !(body instanceof FormData)) {
      headers['Content-Type'] = 'application/json'
    }

    const uiContext = JSON.stringify({
      uiEndPoint: config.UI_END_POINT,
      pollVoteUrl: config.UI_END_POINT + '/poll/{pollId}/vote',
      pollEditUrl: config.UI_END_POINT + '/poll/{pollId}/vote?token={token}',
      resourceUrl: config.END_POINT + '/v1/resources/{resourceId}',
      resourceDownloadUrl: config.END_POINT + '/v1/resources/{resourceId}/download',
      userValidateUrl: config.UI_END_POINT + '/verifyEmail/{userId}/{token}'
    })
    headers['X-Pollen-UI-context'] = uiContext

    return fetch(this.baseUrl + url, {
      headers,
      method,
      credentials: 'include',
      mode: 'cors',
      body:
        body instanceof FormData || typeof body === 'string' ? body : body && JSON.stringify(body)
    }).then((response) => {
      if (response.status === 204) {
        return null
      }
      if (response.status === 200) {
        return response.json()
      }
      if (response.status === 503) {
        return Promise.reject()
      }
      return response.text().then(
        (code) => {
          return Promise.reject(code)
        },
        () => {
          return Promise.reject()
        }
      )
    })
  }

  static get(url: string) {
    const headers = {}
    // headers["Authorization"] = "Bearer " + localStorage.accessToken;
    return this.fetch(url, 'GET', headers)
  }

  static post(url: string, body?: any) {
    const headers = {}
    // headers["Authorization"] = "Bearer " + localStorage.accessToken;
    return this.fetch(url, 'POST', headers, body)
  }

  static postWithHeader(url: string, headers: any, body?: any) {
    return this.fetch(url, 'POST', headers, body)
  }

  static put(url: string, body?: any) {
    const headers = {}
    // headers["Authorization"] = "Bearer " + localStorage.accessToken;
    return this.fetch(url, 'PUT', headers, body)
  }

  static multipartForm(url: string, data: any) {
    let formData: any = null
    if (data) {
      formData = new FormData()

      const keys = Object.keys(data)
      keys.forEach((key) => {
        const value = data[key]
        formData.set(key, value)
      })
    }
    const headers: any = {}
    headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

    return this.fetch(url, 'POST', headers, data)
  }

  static form(url: string, data: any) {
    const params = new URLSearchParams()
    if (data) {
      const keys = Object.keys(data)
      keys.forEach((key) => {
        const value = data[key]
        params.append(key, value)
      })
    }
    const headers: any = {}
    //  headers["Authorization"] = "Bearer " + localStorage.accessToken;
    headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

    return this.fetch(url, 'POST', headers, params.toString())
  }

  static delete(url: string) {
    const headers = {}
    // headers["Authorization"] = "Bearer " + localStorage.accessToken;
    return this.fetch(url, 'DELETE', headers)
  }

  static deleteWithBody(url: string, body: any) {
    const headers = {}
    //  headers["Authorization"] = "Bearer " + localStorage.accessToken;
    return this.fetch(url, 'DELETE', headers, body)
  }

  static parseError(error: any, customI18nKey?: string): string {
    let errorMessage = ''
    console.log(error)
    try {
      const jsonErrors = JSON.parse(error)
      for (const errorKey in jsonErrors) {
        if (customI18nKey) {
          // fix me
          // errorMessage += i18n.t(customI18nKey + '.' + errorKey) + ': '
        }
        errorMessage += jsonErrors[errorKey]
      }
    } catch (e) {
      errorMessage = error
    }

    return errorMessage
  }
}
