import FetchService from '@/services/FetchService'

export function postResendValidation(email: string): Promise<void> {
  const url = 'resendValidation'
  return FetchService.post(url, email)
}
