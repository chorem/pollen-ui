import type { ViewKind } from '@/entities/view-kind'

export function saveViewKindPreferences(ViewKind: ViewKind) {
  localStorage.pollenDashboardViewKind = ViewKind
}

export function getViewKindPreferences(): ViewKind {
  if (localStorage.pollenDashboardViewKind) {
    return localStorage.pollenDashboardViewKind
  }
  return 'MOSAIC'
}
