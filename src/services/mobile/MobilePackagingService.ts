// import { Capacitor, Plugins } from '@capacitor/core'
// import KeyboardManager from './KeyboardManager'
// const { App } = Plugins

/**
 * Service containing all additional logic regarding mobile apps e.g. deep link support, keyboard managment...
 */

/**
 * Here app was opened from external url (e.g. click on a poll link).
 * Routes to the proper screen according to URL.
 * @param app the Vue application
 * @param url the clicked url
 */
// function handleOpeningFromExternalLink(app: Vue, url: string) {
//   const path = new URL(url).pathname
//   // No need to use router.resolve to check url, '404' routes already handles it
//   console.info('[DEEP-LINK] Routing to path ' + path + ' from external url ' + url)
//   app.$router.push(path)
// }

/**
 * Sets up the mobile additional behaviour (deep link support, keyboard management...).
 * @param app the Vue Application
 */
// export function setupUpForMobile(app: Vue) {
//   // Specific natives features
//   // Only installed when we are running on native platform (i.e. Android or iOS app)
//   if (Capacitor.isNative) {
//     // Handle app opening from external links (typically from a poll link)
//     App.addListener('appUrlOpen', (data: any) => {
//       handleOpeningFromExternalLink(app, data.url)
//     })
//   }

//   // Generic mobile features (usefull even if we are on the website)
//   // Handle keyboard opening
//   KeyboardManager.setupKeyboardConfiguration()
// }
