import { config } from '@/config'
import {
  type CommentBean,
  type PaginationResultBean,
  type PollBean,
  type PollenEntityId,
  type PollenEntityRef,
  type Question,
  type Vote,
  type VoteBean,
  type VoteCountingResultBean
} from '@/entities/backend'
import FetchService from '@/services/FetchService'

export default class PollService {
  static pollsPerPage = '12'
  static votesPerPage = '50'
  static commentsPerPage = '10'
  static baseUIUrl = config.UI_END_POINT

  static getPoll(pollId: string, permission: string): Promise<PollBean> {
    let url = 'polls/' + pollId

    const params = new URLSearchParams()
    if (permission) {
      params.append('permission', permission)
      url += '?' + params.toString()
    }

    return FetchService.get(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static getVotes(
    pollId: string,
    questionId: PollenEntityId<Question>,
    permission: string
  ): Promise<PaginationResultBean<VoteBean>> {
    let url = 'polls/' + pollId + '/questions/' + questionId + '/votes'
    const params = new URLSearchParams()
    params.append('order', 'topiaCreateDate')
    params.append('desc', 'true')
    params.append('pageSize', this.votesPerPage)

    if (permission) {
      params.append('permission', permission)
    }

    url += '?' + params.toString()

    return FetchService.get(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static deleteVote(
    poll: PollBean,
    questionId: PollenEntityId<Question>,
    vote: VoteBean
  ): Promise<void> {
    const params = new URLSearchParams()
    params.append(
      'permission',
      vote.permission ? vote.permission : poll.permission ? poll.permission : ''
    )

    const url =
      'polls/' +
      poll.id +
      '/questions/' +
      questionId +
      '/votes/' +
      vote.id +
      '?' +
      params.toString()

    return FetchService.delete(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static addVote(
    poll: PollBean,
    questionId: PollenEntityId<Question>,
    vote: VoteBean
  ): Promise<PollenEntityRef<Vote>> {
    const url = 'polls/' + poll.id + '/questions/' + questionId + '/votes'
    const choices = []
    if (vote.choice) {
      for (const voteValue of vote.choice) {
        const choice = {
          choiceId: voteValue.choiceId,
          voteValue: voteValue.voteValue
        }
        choices.push(choice)
      }
    }

    const voteBean = {
      voterName: vote.voterName ? vote.voterName : '',
      voterAvatar: vote.voterAvatar ? vote.voterAvatar : '',
      permission: vote.permission,
      anonymous: vote.anonymous,
      weight: vote.weight,
      choice: choices
    }

    return FetchService.post(url, voteBean).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static getResults(
    poll: PollBean,
    questionId: PollenEntityId<Question>
  ): Promise<VoteCountingResultBean> {
    if (poll.id) {
      const url = 'polls/' + poll.id + '/questions/' + questionId + '/results'
      return FetchService.get(url).catch((e) => {
        return Promise.reject(FetchService.parseError(e))
      })
    }
    throw new Error('Poll is undefined')
  }

  static editVote(
    poll: PollBean,
    questionId: PollenEntityId<Question>,
    vote: VoteBean,
    permission: string
  ): Promise<any> {
    let url = 'polls/' + poll.id + '/questions/' + questionId + '/votes/' + vote.id

    if (permission) {
      const params = new URLSearchParams()
      params.append('permission', permission)
      url += '?' + params.toString()
    }

    const choices = []
    if (vote.choice) {
      for (const voteValue of vote.choice) {
        const choice = {
          id: voteValue.id,
          choiceId: voteValue.choiceId,
          voteValue: voteValue.voteValue
        }
        choices.push(choice)
      }
    }

    const voteBean = {
      voterName: vote.voterName ? vote.voterName : '',
      voterAvatar: vote.voterAvatar ? vote.voterAvatar : '',
      id: vote.id,
      anonymous: vote.anonymous,
      choice: choices
    }

    return FetchService.post(url, voteBean).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static getVote(
    poll: PollBean,
    questionId: PollenEntityId<Question>,
    voteId: string
  ): Promise<VoteBean> {
    const url = 'polls/' + poll.id + '/questions/' + questionId + '/votes/' + voteId
    return FetchService.get(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static fetchPolls(
    use: string,
    filter: string,
    search: string,
    pageNumber?: string
  ): Promise<PaginationResultBean<PollBean>> {
    const params = new URLSearchParams()
    params.append('use', use)
    params.append('filter', filter)
    params.append('search', search || '')
    params.append('order', 'topiaCreateDate')
    params.append('desc', 'true')
    params.append('pageSize', this.pollsPerPage)

    if (pageNumber) {
      params.append('pageNumber', pageNumber)
    }

    const url = 'polls?' + params.toString()

    return FetchService.get(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static getPolls(
    currentPage: string,
    filter: string,
    search: string,
    pageNumber?: string
  ): Promise<PaginationResultBean<PollBean>> {
    let use = 'ALL'
    switch (currentPage) {
      case 'CREATED':
        use = 'created'
        break
      case 'INVITED':
        use = 'invited'
        break
      case 'PARTICIPATED':
        use = 'participated'
        break
    }

    return this.fetchPolls(use, filter, search, pageNumber)
  }

  static deletePolls(polls: PollBean[]) {
    const promiseArray = []
    for (const poll of polls) {
      const url = 'polls/' + poll.id
      promiseArray.push(FetchService.delete(url))
    }
    return Promise.all(promiseArray).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static clonePoll(poll: PollBean) {
    const url = 'polls/' + poll.id + '/clone'
    return FetchService.post(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static getComments(
    poll: PollBean,
    permission: string,
    pageNumber?: string
  ): Promise<PaginationResultBean<CommentBean>> {
    const params = new URLSearchParams()
    params.append('order', 'topiaCreateDate')
    params.append('desc', 'true')
    params.append('pageSize', this.commentsPerPage)

    if (pageNumber) {
      params.append('pageNumber', pageNumber)
    }

    if (permission) {
      params.append('permission', permission)
    }

    const url = 'polls/' + poll.id + '/comments?' + params.toString()
    return FetchService.get(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static addComment(poll: PollBean, commentBean: CommentBean): Promise<PollenEntityRef<Comment>> {
    const url = 'polls/' + poll.id + '/comments'

    return FetchService.post(url, commentBean).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static deleteComment(poll: PollBean, comment: CommentBean): Promise<void> {
    const params = new URLSearchParams()
    params.append(
      'permission',
      comment.permission ? comment.permission : poll.permission ? poll.permission : ''
    )

    const url = 'polls/' + poll.id + '/comments/' + comment.id + '?' + params.toString()

    return FetchService.delete(url).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static editComment(poll: PollBean, comment: CommentBean): Promise<any> {
    const url = 'polls/' + poll.id + '/comments/' + comment.id

    return FetchService.post(url, comment).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static createPoll(poll: PollBean) {
    const url = 'polls'

    const pollCreationBean = {
      poll: poll,
      voterLists: [],
      voterListMembers: []
    }

    return FetchService.post(url, pollCreationBean).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static editPoll(poll: PollBean) {
    const url = 'polls/' + poll.id
    return FetchService.put(url, poll).catch((e) => {
      return Promise.reject(FetchService.parseError(e))
    })
  }

  static getPollUrl(poll?: PollBean): string {
    if (poll?.id) {
      let pollId = poll.id as any
      if (pollId.id) {
        pollId = pollId.id
      }
      return this.baseUIUrl + '/poll/' + pollId
    }
    return ''
  }

  static linkPoll(pollUrl: string): Promise<any> {
    const pollToAssignUrlPattern = this.baseUIUrl + '/poll/(.+)/vote\\?token=(.+)'
    const regex = new RegExp(pollToAssignUrlPattern)
    const regexpResult = regex.exec(pollUrl.trim())

    if (regexpResult) {
      const pollId = regexpResult[1]
      const permission = regexpResult[2]

      const params = new URLSearchParams()
      params.append('permission', permission ? permission : '')

      const url = 'polls/' + pollId + '/assign' + '?' + params.toString()

      return FetchService.put(url).catch((e) => {
        return Promise.reject(FetchService.parseError(e))
      })
    }
    return Promise.reject('Wrong URL format')
  }
}
