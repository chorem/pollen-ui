import en from '@/locales/en.json'
import fr from '@/locales/fr.json'
import { createI18n } from 'vue-i18n'

type MessageSchema = typeof en

const defineLocale = function () {
  const localLocale = localStorage.locale
  if (localLocale) {
    return localLocale
  } else {
    return navigator.language || 'en'
  }
}

export const i18n = createI18n<[MessageSchema], 'en' | 'fr'>({
  legacy: false,
  locale: defineLocale(),
  fallbackLocale: 'en',
  messages: {
    en,
    fr
  },
  datetimeFormats: {
    en: {
      short: {
        year: 'numeric',
        month: 'short',
        day: 'numeric'
      },
      long: {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        weekday: 'short',
        hour: 'numeric',
        minute: 'numeric'
      }
    },
    fr: {
      short: {
        day: 'numeric',
        month: 'short',
        year: 'numeric'
      },
      long: {
        day: 'numeric',
        month: 'short',
        year: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        hour12: false
      }
    }
  }
})
