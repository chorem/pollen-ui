export const mq = {
  breakpoints: {
    default: 0,
    desktop: 1000
  },
  defaultBreakpoint: 'default'
}
