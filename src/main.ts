import { createApp } from 'vue'
import { Vue3Mq } from 'vue3-mq'
import App from './App.vue'
import { i18n } from './plugins/i18n'
import { mq } from './plugins/mq'
import router from './router'

// // This callback runs before every route change, including on page load.
// // Using it to change title and meta, coming from https://www.digitalocean.com/community/tutorials/vuejs-vue-router-modify-head
// router.beforeEach((to, from, next) => {
//   // This goes through the matched routes from last to first, finding the closest route with a title.
//   // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
//   const nearestWithTitle = to.matched
//     .slice()
//     .reverse()
//     .find(r => r.meta && r.meta.title);
//
//   // Find the nearest route element with meta tags.
//   const nearestWithMeta = to.matched
//     .slice()
//     .reverse()
//     .find(r => r.meta && r.meta.metaTags);
//   const previousNearestWithMeta = from.matched
//     .slice()
//     .reverse()
//     .find(r => r.meta && r.meta.metaTags);
//
//   // If a route with a title was found, set the document (page) title to that value.
//   if (nearestWithTitle)
//     document.title = i18n.t(nearestWithTitle.meta.title);
//
//   // Remove any stale meta tags from the document using the key attribute we set below.
//   Array.from(
//     document.querySelectorAll("[data-vue-router-controlled]")
//   ).map(el => (el.parentNode ? el.parentNode.removeChild(el) : null));
//
//   // Skip rendering meta tags if there are none.
//   if (!nearestWithMeta) return next();
//
//   // Turn the meta tag definitions into actual elements in the head.
//   ((nearestWithMeta.meta.metaTags as [string]).map(tagDef => {
//     const tag = document.createElement("meta");
//
//     Object.keys(tagDef).forEach(key => {
//       // eslint-disable-next-line
//       // @ts-ignore: just want to test
//       tag.setAttribute(key, tagDef[key]);
//     });
//
//     // We use this to track which meta tags we create, so we don't interfere with other ones.
//     tag.setAttribute("data-vue-router-controlled", "");
//
//     return tag;
//   }) as [HTMLMetaElement])
//     // Add the meta tags to the document head.
//     .forEach(tag => document.head.appendChild(tag));
//
//   next();
// });

const app = createApp(App)

app.use(router)
app.use(i18n)
app.use(Vue3Mq, mq)

app.mount('#app')
