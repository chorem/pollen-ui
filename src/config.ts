export interface Config {
  END_POINT: string
  UI_END_POINT: string
}

export const config: Config = {
  END_POINT: import.meta.env.VITE__END_POINT,
  UI_END_POINT: import.meta.env.VITE__UI_END_POINT
}
