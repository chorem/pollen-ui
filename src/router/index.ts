import { isAuthenticated } from '@/services/AuthService'
import { createRouter, createWebHistory } from 'vue-router'
import PollComments from '../components/vote/PollComments.vue'
import PollResults from '../components/vote/PollResults.vue'
import PollSummary from '../components/vote/PollSummary.vue'
import PollVote from '../components/vote/PollVote.vue'
import PollVotes from '../components/vote/PollVotes.vue'
import NotFound404 from '../views/NotFound404.vue'
import PollCreationConfirmation from '../views/poll/PollCreationConfirmation.vue'
import PollEdition from '../views/poll/PollEdition.vue'
import Polls from '../views/polls/Polls.vue'
import CreateAccount from '../views/user/CreateAccount.vue'
import Login from '../views/user/Login.vue'
import Logout from '../views/user/Logout.vue'
import Profile from '../views/user/Profile.vue'
import VerifyAccount from '../views/user/VerifyAccount.vue'
import Poll from '../views/vote/Poll.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/polls'
  },
  {
    path: '/polls',
    name: 'Polls',
    component: Polls,
    meta: {
      title: 'dashboard.Title',
      requiresAuth: true
    }
  },
  {
    path: '/poll/new',
    name: 'PollCreation',
    component: PollEdition,
    meta: {
      title: 'pollEdition.Title'
    }
  },
  {
    path: '/poll/:id/created/:permission',
    name: 'PollCreationConfirmation',
    component: PollCreationConfirmation,
    meta: {
      title: 'pollEdition.Title'
    }
  },
  {
    path: '/poll/:id/edit/:permission/:step?',
    name: 'PollEdition',
    component: PollEdition,
    meta: {
      title: 'pollEdition.Title'
    }
  },
  {
    path: '/poll/:id',
    name: 'Poll',
    component: Poll,
    redirect: { name: 'PollVote' },
    meta: {
      title: 'poll.Title'
    },
    children: [
      {
        path: 'summary',
        name: 'PollSummary',
        component: PollSummary
      },
      {
        path: 'vote',
        name: 'PollVote',
        component: PollVote
      },
      {
        path: 'vote/:voteId',
        name: 'EditVote',
        component: PollVote
      },
      {
        path: 'votes',
        name: 'PollVotes',
        component: PollVotes
      },
      {
        path: 'result',
        name: 'PollResults',
        component: PollResults
      },
      {
        path: 'comments',
        name: 'PollComments',
        component: PollComments
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      title: 'landing.TitleLogin'
    }
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: CreateAccount,
    meta: {
      title: 'landing.CreateAccountTitle'
    }
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    meta: {
      title: 'landing.LogoutTitle'
    }
  },
  {
    path: '/user/profile',
    name: 'UserProfile',
    component: Profile,
    meta: {
      title: 'Pollen - Profile',
      requiresAuth: true
    }
  },
  {
    path: '/verifyEmail/:userId/:token',
    name: 'VerifyAccount',
    component: VerifyAccount,
    props: true,
    meta: {
      title: 'landing.TitleLogin'
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: NotFound404,
    meta: {
      title: 'Pollen - Not found'
    }
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    isAuthenticated().then((authenticated: boolean) => {
      if (authenticated) {
        next()
      } else {
        next({
          name: 'Login',
          params: { redirect: to.fullPath }
        })
      }
    })
  } else {
    next()
  }
})

export default router
