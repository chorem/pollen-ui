export type TemporalFilter = 'NONE' | 'PAST' | 'CURRENT' | 'UPCOMING'
