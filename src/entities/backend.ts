/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.21.588 on 2020-04-17 11:23:15.

export interface ChildFavoriteListBean extends PollenBean<ChildFavoriteList> {
  id?: PollenEntityId<ChildFavoriteList>
  child?: FavoriteListBean
  weight?: number
}

export interface ChoiceBean extends PollenBean<Choice> {
  id?: PollenEntityId<Choice>
  permission?: string
  choiceOrder?: number
  choiceValue?: string
  choiceType?: ChoiceType
  description?: string
  choiceIsDeletable?: boolean
  report?: ReportResumeBean
}

export interface ChoiceScoreBean {
  choiceId?: PollenEntityId<Choice>
  scoreValue?: number
  scoreOrder?: number
}

export interface CommentBean extends PollenBean<Comment> {
  id?: PollenEntityId<Comment>
  permission?: string
  authorName?: string
  authorAvatar?: string
  text?: string
  postDate?: Date
  report?: ReportResumeBean
}

export interface ConfigurationBean {
  userConnectedRequired?: boolean
  usersCanCreatePoll?: UsersRight
}

export interface FavoriteListBean extends PollenBean<FavoriteList> {
  id?: PollenEntityId<FavoriteList>
  name?: string
  countChildren?: number
  countMembers?: number
}

export interface FavoriteListMemberBean extends PollenBean<FavoriteListMember> {
  id?: PollenEntityId<FavoriteListMember>
  name?: string
  email?: string
  weight?: number
}

export type FeedbackCategory = 'BUG' | 'ERGO' | 'OTHER'

export interface FeedbackBean {
  description?: string
  category?: FeedbackCategory
  browser?: string
  operatingSystem?: string
  platform?: string
  screenResolution?: Dimension
  windowDimension?: Dimension
  locale?: Locale
  location?: string
  locationTitle?: string
  screenShotId?: PollenEntityId<PollenResource>
  consoleHistory?: string
  userEmail?: string
}

export interface ListVoteCountingResultBean {
  mainResult?: VoteCountingResultBean
}

export interface LoginProviderBean extends PollenBean<LoginProvider> {
  id?: PollenEntityId<LoginProvider>
  name?: string
  key?: string
  secret?: string
  permissions?: string
  active?: boolean
}

export interface PaginationParameterBean {
  pageNumber?: number
  pageSize?: number
  order?: string
  desc?: boolean
}

export interface PaginationResultBean<O> {
  elements?: O[]
  pagination?: PaginationResultContextBean
}

export interface PollBean {
  id?: string
  permission?: string
  creatorName?: string
  creatorEmail?: string
  title?: string
  description?: string
  createDate?: number
  beginDate?: number
  endDate?: number
  anonymousVoteAllowed?: boolean
  continuousResults?: boolean
  pollType?: PollType
  voteVisibility?: VoteVisibility
  commentVisibility?: CommentVisibility
  resultVisibility?: ResultVisibility
  closed?: boolean
  resultIsVisible?: boolean
  commentIsVisible?: boolean
  voteIsVisible?: boolean
  participantsIsVisible?: boolean
  canVote?: boolean
  commentCount?: number
  invalidEmails?: string[]
  participantCount?: number
  participantInvitedCount?: number
  status?: PollStatus
  notifyMeHoursBeforePollEnds?: number
  voteNotification?: boolean
  commentNotification?: boolean
  newChoiceNotification?: boolean
  notificationLocale?: string
  report?: ReportResumeBean
  gtuValidated?: boolean
  maxVoters?: number
  creatorAvatar?: string
  emailAddressSuffixes?: string[]
  questions?: QuestionBean[]
  picture?: string
  builtinPicture?: string
  resultPresentation?: ResultPresentation
}

export interface PollenBean<E> {
  id?: PollenEntityId<E>
}

export interface PollenBeans {}

export interface PollenEntityId<E> {
  entityId?: string
  reducedId?: string
  notEmpty?: boolean
  temporaryId?: boolean
  empty?: boolean
}

export interface PollenEntityRef<E> extends PollenEntityId<E> {
  permission?: string
}

export interface PollenStatus {
  version?: string
  buildDate?: string
  encoding?: string
  persistenceOk?: boolean
  nbPolls?: number
  errors?: string[]
  allOk?: boolean
  memoryAllocated?: string
  memoryUsed?: string
  memoryFree?: string
  memoryMax?: string
  loadAverage?: number
  duration?: number
}

export interface PollenUserBean extends PollenBean<PollenUser> {
  id?: PollenEntityId<PollenUser>
  name?: string
  administrator?: boolean
  language?: string
  password?: string
  banned?: boolean
  emailIsValidate?: boolean
  withPassword?: boolean
  credentials?: UserCredentialBean[]
  gtuValidated?: boolean
  avatar?: string
  premiumTo?: Date
  premium?: boolean
  emailAddresses?: PollenUserEmailAddressBean[]
  defaultEmailAddress?: PollenUserEmailAddressBean
  canCreatePoll?: boolean
}

export interface PollenUserEmailAddressBean extends PollenBean<PollenUserEmailAddress> {
  id?: PollenEntityId<PollenUserEmailAddress>
  emailAddress?: string
  validated?: boolean
  pgpPublicKey?: string
}

export interface QuestionBean extends PollenBean<Question> {
  id?: PollenEntityId<Question>
  permission?: string
  createDate?: number
  title?: string
  description?: string
  beginChoiceDate?: number
  endChoiceDate?: number
  choiceAddAllowed?: boolean
  voteCountingType?: number
  voteCountingConfig?: VoteCountingConfig
  poll?: string
  resultIsVisible?: boolean
  commentIsVisible?: boolean
  voteIsVisible?: boolean
  participantsIsVisible?: boolean
  choices?: ChoiceBean[]
  choiceCount?: number
  voteCount: number
  commentCount?: number
  questionOrder?: number
  canVote?: boolean
  report?: ReportResumeBean
}

export interface ReportBean extends PollenBean<Report> {
  id?: PollenEntityId<Report>
  level?: ReportLevel
  email?: string
  ignore?: boolean
}

export interface ReportResumeBean {
  score?: number
  count?: number
  ignore?: number
}

export interface UserCredentialBean extends PollenBean<UserCredential> {
  id?: PollenEntityId<UserCredential>
  provider?: string
  userName?: string
  emailAddress?: string
}

export interface VoteBean extends PollenBean<Vote> {
  id?: PollenEntityId<Vote>
  voterId?: PollenEntityId<PollenPrincipal>
  voterName?: string
  voterAvatar?: string
  permission?: string
  anonymous?: boolean
  weight?: number
  choice?: VoteToChoiceBean[]
  report?: ReportResumeBean
  ignored?: boolean
  voterListMemberId?: PollenEntityId<VoterListMember>[]
}

export interface VoteCountingGroupResultBean {
  scores?: ChoiceScoreBean[]
  nbVotants?: number
  groupId?: PollenEntityId<VoterList>
}

export interface VoteCountingResultBean {
  scores?: ChoiceScoreBean[]
  detail?: VoteCountingDetailResultBean
  nbVotants?: number
}

export interface VoteCountingTypeBean {
  id?: number
  name?: string
  helper?: string
  shortHelper?: string
  renderType?: string
  minimumValue?: number
}

export interface VoteToChoiceBean extends PollenBean<VoteToChoice> {
  id?: PollenEntityId<VoteToChoice>
  choiceId?: PollenEntityId<Choice>
  voteValue?: number | string
}

export interface VoterListBean extends PollenBean<VoterList> {
  id?: PollenEntityId<VoterList>
  name?: string
  weight?: number
  parentId?: PollenEntityId<VoterList>
  countSubLists?: number
  countMembers?: number
  allEmails?: string[]
}

export interface VoterListMemberBean extends PollenBean<VoterListMember> {
  id?: PollenEntityId<VoterListMember>
  name?: string
  avatar?: string
  email?: string
  weight?: number
  voterListId?: PollenEntityId<VoterList>
  invitationSent?: boolean
  voting?: boolean
  invalidEmail?: boolean
}

export interface ChoiceIdAble {
  choiceId?: string
}

export interface ChoiceScore extends ChoiceIdAble, Comparable<ChoiceScore>, Serializable {
  scoreValue?: number
  scoreOrder?: number
}

export interface EmptyVoteCountingConfig extends VoteCountingConfig {}

export interface ListOfVoter extends Voter, Iterable<Voter> {
  voters?: Voter[]
  result?: VoteCountingResult
}

export interface ListVoteCountingResult extends Serializable {
  groupIds?: string[]
  mainResult?: VoteCountingResult
}

export interface MinMaxChoicesNumberConfig extends VoteCountingConfig {
  maxChoiceNumber?: number
  minChoiceNumber?: number
}

export interface SimpleVoter extends Voter {}

export interface SimpleVoterBuilder extends VoterBuilder {}

export interface VoteCountingConfig {}

export interface VoteCountingDetailResult extends Serializable {}

export interface VoteCountingResult extends Serializable {
  scores?: ChoiceScore[]
  nbVotants?: number
  detailResult?: VoteCountingDetailResult
  scoresByRank?: ArrayListMultimap<number, ChoiceScore>
  topRanking?: ChoiceScore[]
}

export interface VoteForChoice extends ChoiceIdAble, Serializable {
  voteValue?: number
}

export interface Voter extends Serializable {
  weight?: number
  voteForChoices?: VoteForChoice[]
  voterId?: string
}

export interface VoterBuilder {
  voters?: Voter[]
}

export interface ChildFavoriteList extends TopiaEntity {
  child?: FavoriteList
  weight?: number
  parent?: FavoriteList
}

export interface Choice extends TopiaEntity {
  description?: string
  question?: Question
  choiceValue?: string
  choiceType?: ChoiceType
  creator?: PollenPrincipal
  choiceOrder?: number
}

export interface Comment extends TopiaEntity {
  text?: string
  question?: Question
  poll?: Poll
  postDate?: Date
  author?: PollenPrincipal
}

export interface FavoriteList extends TopiaEntity {
  owner?: PollenPrincipal
  parentLists?: FavoriteList
  pollenUser?: PollenUser
  name?: string
}

export interface FavoriteListMember extends TopiaEntity {
  weight?: number
  email?: string
  favoriteList?: FavoriteList
  name?: string
}

export interface Dimension extends Dimension2D, Serializable {}

export interface Locale extends Cloneable, Serializable {}

export interface PollenResource extends TopiaEntity {
  contentType?: string
  resourceType?: ResourceType
  resourceContent?: Blob
  name?: string
  size?: number
}

export interface LoginProvider extends TopiaEntity {
  active?: boolean
  permissions?: string
  key?: string
  secret?: string
  name?: string
}

export interface PaginationResultContextBean {
  count?: number
  currentPage?: number
  lastPage?: number
  pageSize?: number
  order?: string
  desc?: boolean
}

export interface Poll extends TopiaEntity {
  description?: string
  title?: string
  feedContent?: string
  beginDate?: Date
  endDate?: Date
  anonymousVoteAllowed?: boolean
  continuousResults?: boolean
  pollType?: PollType
  voteVisibility?: VoteVisibility
  commentVisibility?: CommentVisibility
  resultVisibility?: ResultVisibility
  creator?: PollenPrincipal
  premium?: boolean
  notifyMeHoursBeforePollEnds?: number
  voteNotification?: boolean
  commentNotification?: boolean
  newChoiceNotification?: boolean
  notificationLocale?: string
  emailAddressSuffixes?: string
  pollEndReminderSent?: boolean
  gtuValidationDate?: Date
  notificationMaxVoterSend?: boolean
  picture?: string
}

export interface PollenUser extends TopiaEntity {
  salt?: string
  password?: string
  language?: string
  avatar?: PollenResource
  premiumTo?: Date
  emailAddresses?: PollenUserEmailAddress[]
  defaultEmailAddress?: PollenUserEmailAddress
  canCreatePoll?: boolean
  administrator?: boolean
  banned?: boolean
  gtuValidationDate?: Date
  userCredential?: UserCredential[]
  userCredentialTopiaIds?: string[]
  userCredentialEmpty?: boolean
  userCredentialNotEmpty?: boolean
  emailAddressesTopiaIds?: string[]
  emailAddressesEmpty?: boolean
  emailAddressesNotEmpty?: boolean
  emailValidated?: boolean
  name?: string
}

export interface PollenUserEmailAddress extends TopiaEntity {
  emailAddress?: string
  validated?: boolean
  pgpPublicKey?: string
}

export interface Question extends TopiaEntity {
  description?: string
  title?: string
  voteCountingConfig?: string
  poll?: Poll
  questionOrder?: number
  beginChoiceDate?: Date
  endChoiceDate?: Date
  choiceAddAllowed?: boolean
  voteCountingType?: number
}

export interface Report extends TopiaEntity {
  targetId?: string
  level?: number
  email?: string
  ignore?: boolean
}

export interface UserCredential extends TopiaEntity {
  provider?: string
  userName?: string
  email?: string
  userId?: string
}

export interface Vote extends TopiaEntity {
  voterListMemberTopiaIds?: string[]
  voterListMemberEmpty?: boolean
  voterListMemberNotEmpty?: boolean
  voteToChoice?: VoteToChoice[]
  weight?: number
  question?: Question
  anonymous?: boolean
  voter?: PollenPrincipal
  voteToChoiceTopiaIds?: string[]
  voteToChoiceEmpty?: boolean
  voteToChoiceNotEmpty?: boolean
  voterListMember?: VoterListMember[]
}

export interface PollenPrincipal extends TopiaEntity {
  pollenUser?: PollenUser
  email?: string
  invalid?: boolean
  name?: string
  permission?: PollenToken
}

export interface VoterListMember extends TopiaEntity {
  member?: PollenPrincipal
  weight?: number
  invitationSent?: boolean
  voterList?: VoterList
}

export interface VoterList extends TopiaEntity {
  weight?: number
  poll?: Poll
  name?: string
  parent?: VoterList
}

export interface VoteCountingDetailResultBean {}

export interface VoteToChoice extends TopiaEntity {
  voteValue?: number
  choice?: Choice
}

export interface Serializable {}

export interface ArrayListMultimap<K, V>
  extends ArrayListMultimapGwtSerializationDependencies<K, V> {}

export interface TopiaEntity extends Serializable {
  topiaId?: string
  topiaVersion?: number
  topiaCreateDate?: Date
  persisted?: boolean
  deleted?: boolean
}

export interface Dimension2D extends Cloneable {
  width?: number
  height?: number
}

export interface Cloneable {}

export interface Blob {
  binaryStream?: any
}

export interface PollenToken extends TopiaEntity {
  token?: string
  creationDate?: Date
  endDate?: Date
}

export interface Comparable<T> {}

export interface Iterable<T> {}

export interface ArrayListMultimapGwtSerializationDependencies<K, V>
  extends AbstractListMultimap<K, V> {}

export interface AbstractListMultimap<K, V>
  extends AbstractMapBasedMultimap<K, V>,
    ListMultimap<K, V> {}

export interface AbstractMapBasedMultimap<K, V> extends AbstractMultimap<K, V>, Serializable {}

export interface ListMultimap<K, V> extends Multimap<K, V> {}

export interface AbstractMultimap<K, V> extends Multimap<K, V> {}

export interface Multimap<K, V> {
  empty?: boolean
}

export const enum ReportLevel {
  OFF_TOPIC = 'OFF_TOPIC',
  SPAM = 'SPAM',
  ILLEGAL = 'ILLEGAL'
}

export const enum UsersRight {
  ALL_USERS = 'ALL_USERS',
  USERS_CONNECTED = 'USERS_CONNECTED',
  USERS_SELECTED = 'USERS_SELECTED'
}

export const enum ChoiceToVoteRenderType {
  CHECKBOX = 'CHECKBOX',
  TEXTFIELD = 'TEXTFIELD',
  SELECT = 'SELECT'
}

export const enum ChoiceType {
  TEXT = 'TEXT',
  DATE = 'DATE',
  RESOURCE = 'RESOURCE',
  DATETIME = 'DATETIME'
}

export const enum PollType {
  FREE = 'FREE',
  RESTRICTED = 'RESTRICTED',
  REGISTERED = 'REGISTERED'
}

export const enum VoteVisibility {
  ANONYMOUS = 'ANONYMOUS',
  CREATOR = 'CREATOR',
  VOTER = 'VOTER',
  EVERYBODY = 'EVERYBODY'
}

export const enum CommentVisibility {
  CREATOR = 'CREATOR',
  VOTER = 'VOTER',
  EVERYBODY = 'EVERYBODY'
}

export const enum ResultVisibility {
  CREATOR = 'CREATOR',
  VOTER = 'VOTER',
  EVERYBODY = 'EVERYBODY'
}

export const enum PollStatus {
  CREATED = 'CREATED',
  VOTING = 'VOTING',
  ADDING_CHOICES = 'ADDING_CHOICES',
  CLOSED = 'CLOSED'
}

export const enum ResourceType {
  CHOICE = 'CHOICE',
  SCREENSHOT = 'SCREENSHOT',
  GTU = 'GTU',
  AVATAR = 'AVATAR',
  HEADER = 'HEADER'
}

export type ResultPresentation = 'TROPHY' | 'BALLOT_BOX'
