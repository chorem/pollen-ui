export enum VoteCountingType {
  NORMAL = 1,
  CUMULATIVE = 2,
  CONDORCET = 3,
  NUMBER = 4,
  BORDA = 5,
  INSTANT_RUN_OFF = 6,
  COOMBS = 7,
  MAJORITY_JUDGMENT = 8
}

export function getVoteCountingTypeFromString(typeName: string): VoteCountingType {
  switch (typeName) {
    case 'normal':
      return VoteCountingType.NORMAL
    case 'cumulative':
      return VoteCountingType.CUMULATIVE
    case 'condorcet':
      return VoteCountingType.CONDORCET
    case 'number':
      return VoteCountingType.NUMBER
    case 'borda':
      return VoteCountingType.BORDA
    case 'instantRunOff':
      return VoteCountingType.INSTANT_RUN_OFF
    case 'coombs':
      return VoteCountingType.COOMBS
    case 'majorityJudgment':
      return VoteCountingType.MAJORITY_JUDGMENT
    default:
      return VoteCountingType.NORMAL
  }
}
