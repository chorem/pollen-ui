<script setup lang="ts">
defineProps<{
  isactive?: boolean
  href?: string
  icon?: string
  label: string
}>()

const emit = defineEmits<{ clicked: [] }>()

function onclick() {
  emit('clicked')
}
</script>

<template>
  <div :class="{ active: isactive }">
    <a
      :href="href"
      :class="{ active: isactive }"
      @click="onclick"
      @keyup.enter="onclick"
      tabindex="0"
    >
      <span>
        <i :class="icon" class="icon" v-if="icon"></i>
        {{ label }}
      </span>
      <div class="selector"></div>
    </a>
  </div>
</template>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped lang="less">
a {
  display: flex;
  flex-direction: column;

  font-family:
    Open sans,
    sans-serif;
  font-style: normal;
  text-align: left;
  text-decoration: none;
  // outline: none;
  padding: 7px;

  cursor: pointer;

  .icon {
    vertical-align: baseline;
  }

  &.active {
    // color: var(--color-link--hover);
    // font-weight: 600;
    // border-color: var(--color-link--hover);
    background-color: #C4CDCD;
  }

  &:not(.active):hover {
    color: var(--color-link--hover);
    background-color: #EAEDEC;
  }

  &:visited {
    color: inherit;
  }
}

// .selector {
//   top: 213px;
//   left: 50px;

//   height: 2px;
//   margin-top: 2px;

//   background-color: var(--color-link--hover);
//   border-radius: 2px;

//   visibility: hidden;
// }

// .active .selector {
//   visibility: visible;
// }

i {
  display: inline-flex;
  align-items: center;
  justify-content: center;

  width: 20px;
  height: 20px;

  margin-right: 10px;

  color: var(--color-link--hover);
  font-size: 20px;
  vertical-align: middle;
}
</style>
